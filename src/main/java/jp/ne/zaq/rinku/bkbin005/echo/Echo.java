/**
 * Echo program main.
 */
package jp.ne.zaq.rinku.bkbin005.echo;

import jp.ne.zaq.rinku.bkbin005.args.Args;

/**
 * My echo command.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 */
public final class Echo {
    /**
     * can not create instance.
     */
    private Echo() { }
    /**
     * main.
     * @param args arguments.
     */
    public static void main(final String[] args) {
        System.out.println(new Args(args));
    }
}
