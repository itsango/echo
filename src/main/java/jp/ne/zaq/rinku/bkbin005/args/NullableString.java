/**
 * Nullable String file.
 */
package jp.ne.zaq.rinku.bkbin005.args;

/**
 * Nullable String.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 */
public final class NullableString {
    /** value. */
    private String str;

    /**
     * Default constructor.
     */
    public NullableString() {
        super();
    }

    /**
     * constructor.
     * @param lstr argument.
     */
    public NullableString(final String lstr) {
        super();
        this.str = lstr;
    }

    /**
     * Get String.
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (this.str == null) {
            return "";
        }
        return this.str;
    }

    /**
     * getter.
     * @return the str.
     */
    public String get() {
        return str;
    }

    /**
     * settter.
     * @param lstr the str to set.
     */
    public void set(final String lstr) {
        this.str = lstr;
    }
}
