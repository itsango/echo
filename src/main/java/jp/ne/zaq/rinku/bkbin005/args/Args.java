/**
 * Args class file.
 */
package jp.ne.zaq.rinku.bkbin005.args;

/**
 * Args class.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 *
 */
public final class Args {

    /** data. */
    private String[] args;

    /**
     * Constructor.
     */
    public Args() {
        super();
    }

    /**
     * Constructor.
     * @param largs arguments
     */
    public Args(final String[] largs) {
        super();
        if (largs == null) {
            this.args = null;
        } else {
            this.args = largs.clone();
        }
    }

    /**
     * getter.
     * @return the args
     */
    public String[] get() {
        if (this.args == null) {
            return null;
        }
        return this.args.clone();
    }

    /**
     * setter.
     * @param largs the args to set.
     */
    public void set(final String[] largs) {
        if (largs == null) {
            this.args = null;
        } else {
            this.args = largs.clone();
        }
    }

    /**
     * get value as String.
     */
    @Override
    public String toString() {
        if (args == null) {
            return "";
        }
        if (args.length < 1) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        buf.append(new NullableString(args[0]));
        for (int i = 1; i < args.length; ++i) {
            buf.append(' ');
            buf.append(new NullableString(args[i]));
        }
        return buf.toString();
    }
}
