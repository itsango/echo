/**
 * NullableString test file.
 */
package jp.ne.zaq.rinku.bkbin005.args;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test NullableString.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 */
public class NullableStringTest {

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.NullableString#NullableString()}.
     */
    @Test
    public void testNullableString() {
        NullableString ns = new NullableString();
        assertNull(ns.get());
    }

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.NullableString#NullableString(java.lang.String)}.
     */
    @Test
    public void testNullableStringString() {
        final String ARG = "abc";
        NullableString ns = new NullableString(ARG);
        assertThat(ns.get(), is(ARG));
    }

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.NullableString#toString()}.
     */
    @Test
    public void testToString() {
        NullableString ns = new NullableString(null);
        assertThat(ns.toString(), is(""));
        final String ARG = ";cp /etc/passwd '文字';";
        ns = new NullableString(ARG);
        assertThat(ns.toString(), is(ARG));
    }

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.NullableString#get()}.
     */
    @Test
    public void testGet() {
        NullableString ns = new NullableString();
        ns.set(null);
        assertNull(ns.get());
        final String ARG = "<script>alert('文字')</script>";
        ns.set(ARG);
        assertThat(ns.get(), is(ARG));
    }

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.NullableString#set(java.lang.String)}.
     */
    @Test
    public void testSet() {
        final String ARG = "漢字";
        NullableString ns = new NullableString();
        ns.set(ARG);
        assertThat(ns.get(), is(ARG));
    }

}
