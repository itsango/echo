/**
 * 
 */
package jp.ne.zaq.rinku.bkbin005.echo;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

/**
 * Test echo.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 */
public class EchoTest {
    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.echo.Echo#main(java.lang.String[])}.
     */
    @Test
    public void testMain() {
        ByteArrayOutputStream bas = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bas));
        Echo.main(new String[] { "x", "y"});
        assertThat(bas.toString(), is("x y\n"));
    }

}
