/**
 * Test for Args.
 */
package jp.ne.zaq.rinku.bkbin005.args;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

import jp.ne.zaq.rinku.bkbin005.args.Args;

/**
 * test Args.
 * @author Mitsutoshi NAKANO <ItSANgo@gmail.com>
 */
public class ArgsTest {

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#Args()}.
     */
    @Test
    public void testArgs() {
        Args args = new Args();
        assertNull(args.get());
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#Args()}.
     */
    @Test
    public void testArgsString1hasValue() {
        Args args = new Args(new String[] { "a", "b" } );
        assertThat(args.toString(), is("a b"));
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#Args()}.
     */
    @Test
    public void testArgsString2null() {
        Args args = new Args(null);
        assertThat(args.toString(), is(""));
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#get()}.
     */
    @Test
    public void testGet() {
        final String[] args = { "a", "b" };
        final Args arguments = new Args(args);
        assertThat(arguments.get(), is(args));
    }

    /**
     * Test method for
     * {@link jp.ne.zaq.rinku.bkbin005.args.Args#set(java.lang.String[])}.
     */
    @Test
    public void testSet() {
        final String[] args = null;
        Args arguments = new Args();
        arguments.set(args);
        assertThat(arguments.get(), is(args));
        final String[] args2 = { "a", "b" };
        arguments.set(args2);
        assertThat(arguments.get(), is(args2));
        
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#toString()}.
     */
    @Test
    public void testToString01length2() {
        final String[] args = { "a", "b" };
        final Args arguments = new Args(args);
        final String ACTUAL = arguments.toString();
        System.err.println(ACTUAL);
        assertThat(ACTUAL, is("a b"));
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#toString()}.
     */
    @Test
    public void testToString02length0() {
        final String[] args = {};
        final Args arguments = new Args(args);
        final String ACTUAL = arguments.toString();
        System.err.println(ACTUAL);
        assertThat(ACTUAL, is(""));
    }

    /**
     * Test method for {@link jp.ne.zaq.rinku.bkbin005.args.Args#toString()}.
     */
    @Test
    public void testToString03null() {
        final String[] args = null;
        final Args arguments = new Args(args);
        final String ACTUAL = arguments.toString();
        System.err.println(ACTUAL);
        assertThat(ACTUAL, is(""));
    }
}
